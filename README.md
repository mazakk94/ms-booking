### About
Simple Java application which 
1. exposes API for 
- displaying rooms availability, 
- booking rooms 
- deleting bookings

2. sends notification day before checkIn to user which booked room

### Usage
Build jar and run locally -replace $PROJECT_ROOT with path to project root (for example `~/repos/ms-booking`)

```cd $PROJECT_ROOT && mvn clean install && java -jar target/booking-0.0.1-SNAPSHOT.jar```

### API HTTP Endpoints - Postman Collection to import
`https://www.getpostman.com/collections/d54e38bb5c5a97ee7244`

### Swagger UI Rest client
`http://localhost:8080/swagger-ui.html`

### Additional info
- There are 6 rooms initialized with ids from 1 to 6
- Date format example `2020-02-03`