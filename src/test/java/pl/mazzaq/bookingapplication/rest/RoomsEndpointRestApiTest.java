package pl.mazzaq.bookingapplication.rest;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpMethod.GET;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RoomsEndpointRestApiTest {

    static final String API_ROOMS = "/api/rooms";

    @LocalServerPort
    int port;

    @Autowired
    TestRestTemplate restTemplate;

    @Test
    void shouldReturnOkWhenValidDatesProvided() {
        //given
        String roomsApiUrl = UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/rooms")
                .queryParam("checkIn", "2020-05-01")
                .queryParam("checkOut", "2020-05-06").toUriString();

        //when
        ResponseEntity<String> response = callHttpGetUrl(roomsApiUrl);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }


    @Test
    void shouldReturnBadRequestWhenOnlyCheckInProvided() {
        //given
        String roomsApiUrl = UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/rooms")
                .queryParam("checkIn", "2020-05-01").toUriString();

        //when
        ResponseEntity<String> response = callHttpGetUrl(roomsApiUrl);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void shouldReturnBadRequestWhenCheckOutIsNotAfterCheckIn() {
        //given
        String roomsApiUrl = UriComponentsBuilder.fromHttpUrl(getLocalhostURL() + API_ROOMS)
                .queryParam("checkIn", "2020-05-01")
                .queryParam("checkOut", "2020-04-06").toUriString();

        //when
        ResponseEntity<String> response = callHttpGetUrl(roomsApiUrl);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void shouldReturnBadRequestWhenCheckInIsNull() {
        //given
        String roomsApiUrl = UriComponentsBuilder.fromHttpUrl(getLocalhostURL() + API_ROOMS)
                .queryParam("checkOut", "2020-04-06").toUriString();

        //when
        ResponseEntity<String> response = callHttpGetUrl(roomsApiUrl);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    void shouldReturnBadRequestWhenInvalidDateFormatProvided() {
        //given
        String roomsApiUrl = UriComponentsBuilder.fromHttpUrl(getLocalhostURL() + API_ROOMS)
                .queryParam("checkIn", "2020/04/06").toUriString();

        //when
        ResponseEntity<String> response = callHttpGetUrl(roomsApiUrl);

        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<String> callHttpGetUrl(String roomsApiUrl) {
        return restTemplate.exchange(
                roomsApiUrl,
                GET,
                getBasicHttpEntity(),
                String.class);
    }

    private String getLocalhostURL() {
        return "http://localhost:" + port;
    }

    private HttpEntity<Object> getBasicHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        return new HttpEntity<>(headers);
    }

    //todo tests for deletion

}
