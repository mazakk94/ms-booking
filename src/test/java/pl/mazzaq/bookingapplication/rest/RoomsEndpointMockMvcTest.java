package pl.mazzaq.bookingapplication.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RoomsEndpointMockMvcTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void shouldReturnOkWhenValidTimeRangeProvided() throws Exception {
        mockMvc.perform(get("/api/rooms")
                .queryParam("checkIn", "2020-06-01")
                .queryParam("checkOut", "2020-06-06"))
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturnBadRequestWhenOnlyCheckInProvided() throws Exception {
        mockMvc.perform(get("/api/rooms")
                .queryParam("checkIn", "2018-01-01"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldReturnValidationErrorWhenCheckInNotProvided() throws Exception {
        mockMvc.perform(get("/api/rooms")
                .queryParam("checkOut", "2020-05-06"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldReturnValidationErrorWhenCheckInIsAfterCheckOut() throws Exception {
        mockMvc.perform(get("/api/rooms")
                .queryParam("checkIn", "2020-06-01")
                .queryParam("checkOut", "2020-05-06"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldReturnValidationErrorWhenInvalidDateFormatProvided() throws Exception {
        mockMvc.perform(get("/api/rooms")
                .queryParam("checkIn", "06/01/2020"))
                .andExpect(status().isBadRequest());
    }

    //todo tests for deletion
}
