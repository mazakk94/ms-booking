package pl.mazzaq.bookingapplication.common.dto.input.validation;

import org.junit.jupiter.api.Test;
import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;

import java.util.Date;

import static java.util.concurrent.TimeUnit.DAYS;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DateCriteriaValidatorUnitTest {

    static long TIME = 1587842051; // 25 April 2020 21:15
    DateCriteriaValidator validator = new DateCriteriaValidator();

    @Test
    void shouldReturnTrueWhenValidDateCriteriaProvided() {
        DateCriteria criteria = new DateCriteria(new Date(TIME), new Date(TIME + DAYS.toMillis(3)));
        assertTrue(validator.isValid(criteria, null));
    }

    @Test
    void shouldReturnFalseWhenNoParamsProvided() {
        DateCriteria criteria = new DateCriteria(null, null);
        assertFalse(validator.isValid(criteria, null));
    }

    @Test
    void shouldReturnFalseWhenCheckOutIsNull() {
        DateCriteria criteria = new DateCriteria(new Date(TIME), null);
        assertFalse(validator.isValid(criteria, null));
    }

    @Test
    void shouldReturnFalseWhenCheckInIsNull() {
        DateCriteria criteria = new DateCriteria(null, new Date(TIME));
        assertFalse(validator.isValid(criteria, null));
    }

    @Test
    void shouldReturnFalseWhenCheckInIsAfterCheckOut() {
        DateCriteria criteria = new DateCriteria(new Date(TIME + DAYS.toMillis(3)), new Date(TIME));
        assertFalse(validator.isValid(criteria, null));
    }
}