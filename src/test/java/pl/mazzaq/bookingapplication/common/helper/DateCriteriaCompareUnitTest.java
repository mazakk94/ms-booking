package pl.mazzaq.bookingapplication.common.helper;

import org.junit.jupiter.api.Test;
import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;

import java.util.Date;

import static org.junit.Assert.assertEquals;

class DateCriteriaCompareUnitTest {

    static final Date _22_APRIL = new Date(1587506400000L); // 22 April 2020
    static final Date _28_APRIL = new Date(1588024800000L); // 28 April 2020
    static final Date _30_APRIL = new Date(1588197600000L); // 30 April 2020
    static final Date _7_MAY = new Date(1588813908572L); // 7 May 2020


    @Test
    void shouldReturnMinusOneWhenDateIsBefore() {
        assertEquals(-1, new DateCriteria(_22_APRIL, _28_APRIL).compareTo(new DateCriteria(_30_APRIL, _7_MAY)));
    }

    @Test
    void shouldReturnMinusOneWhenLaterCheckInIsEqualToEarlierCheckOut() {
        assertEquals(-1, new DateCriteria(_22_APRIL, _28_APRIL).compareTo(new DateCriteria(_28_APRIL, _7_MAY)));
    }


    @Test
    void shouldReturnPlusOneWhenDateIsAfter() {
        assertEquals(1, new DateCriteria(_30_APRIL, _7_MAY).compareTo(new DateCriteria(_22_APRIL, _28_APRIL)));
    }

    @Test
    void shouldReturnPlusOneWhenLaterCheckInIsEqualToEarlierCheckOut() {
        assertEquals(1, new DateCriteria(_28_APRIL, _7_MAY).compareTo(new DateCriteria(_22_APRIL, _28_APRIL)));
    }

    @Test
    void shouldReturnZeroWhenCheckInIsEqual() {
        assertEquals(0, new DateCriteria(_22_APRIL, _7_MAY).compareTo(new DateCriteria(_22_APRIL, _28_APRIL)));
    }

    @Test
    void shouldReturnZeroWhenCheckInIsEqual2() {
        assertEquals(0, new DateCriteria(_22_APRIL, _28_APRIL).compareTo(new DateCriteria(_22_APRIL, _7_MAY)));
    }

    @Test
    void shouldReturnZeroWhenDateIsInside() {
        assertEquals(0, new DateCriteria(_22_APRIL, _7_MAY).compareTo(new DateCriteria(_28_APRIL, _30_APRIL)));
    }

    @Test
    void shouldReturnZeroWhenDateIsInside2() {
        assertEquals(0, new DateCriteria(_28_APRIL, _30_APRIL).compareTo(new DateCriteria(_22_APRIL, _7_MAY)));

    }

    @Test
    void shouldReturnZeroWhenDateIsBetweenWithCheckInEarlier() {
        assertEquals(0, new DateCriteria(_22_APRIL, _30_APRIL).compareTo(new DateCriteria(_28_APRIL, _7_MAY)));
    }

    @Test
    void shouldReturnZeroWhenDateIsBetweenWithCheckInLater() {
        assertEquals(0, new DateCriteria(_28_APRIL, _7_MAY).compareTo(new DateCriteria(_22_APRIL, _30_APRIL)));
    }
}