package pl.mazzaq.bookingapplication;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.mazzaq.bookingapplication.rooms.rest.RoomsEndpoint;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class BookingApplicationTests {

    @Autowired
    private RoomsEndpoint roomsEndpoint;

    @Test
    void contextLoads() {
        //todo inject all beans
        assertThat(roomsEndpoint).isNotNull();
    }
}
