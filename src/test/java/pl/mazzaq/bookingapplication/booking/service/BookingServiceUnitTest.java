package pl.mazzaq.bookingapplication.booking.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.mazzaq.bookingapplication.booking.dto.input.BookingRequest;
import pl.mazzaq.bookingapplication.booking.repository.BookingRepository;
import pl.mazzaq.bookingapplication.booking.repository.model.Booking;
import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;
import pl.mazzaq.bookingapplication.common.rest.exception.RoomNotFoundException;
import pl.mazzaq.bookingapplication.rooms.repository.RoomRepository;
import pl.mazzaq.bookingapplication.rooms.repository.model.Room;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;


class BookingServiceUnitTest {

    static final Room ROOM_ONE = new Room(1L, "room one");
    static final long TIME = 1588252684745L; // 30 April 2020
    static final long TWO_DAYS = 172800000L;
    static final String EMAIL = "jan.kowalski@gmail.com";
    static final Date CHECK_IN = new Date(TIME);
    static final Date CHECK_OUT = new Date(TIME + TWO_DAYS);
    static final long NOT_EXISTING_ROOM_ID = 2L;

    BookingRepository bookingRepository = mock(BookingRepository.class);
    RoomRepository roomRepository = mock(RoomRepository.class);

    BookingService sut = new BookingService(bookingRepository, roomRepository);

    @BeforeEach
    void setUp() {
        initializeRooms();
    }

    @Test
    void shouldCreateBookingWhenRoomExistsAndIsAvailable() {
        // given
        BookingRequest request = new BookingRequest(ROOM_ONE.getId(), new DateCriteria(CHECK_IN, CHECK_OUT), EMAIL);
        Booking expectedBooking = Booking.builder()
                .id(ROOM_ONE.getId())
                .checkIn(CHECK_IN)
                .checkOut(CHECK_OUT)
                .email(EMAIL)
                .room(ROOM_ONE)
                .build();
        expectSavedBooking(expectedBooking);

        // when
        Booking booking = sut.createBooking(request);

        //then
        assertThat(booking).isEqualTo(expectedBooking);
    }

    @Test
    void shouldThrowRoomNotFoundExceptionWhenNotExistingRoomIdProvided() {
        // given
        BookingRequest request = new BookingRequest(NOT_EXISTING_ROOM_ID, new DateCriteria(CHECK_IN, CHECK_OUT), EMAIL);

        // when, then
        assertThrows(RoomNotFoundException.class, () -> sut.createBooking(request));
    }

    @Test
    void shouldThrowRoomNotAvailableExceptionWhenRoomIsOccupied() {
        // given
        BookingRequest request = new BookingRequest(NOT_EXISTING_ROOM_ID, new DateCriteria(CHECK_IN, CHECK_OUT), EMAIL);

        // when, then
        assertThrows(RoomNotFoundException.class, () -> sut.createBooking(request));
    }

    @Test
    void shouldReturnTrueWhenThereAreNoBookingsInGivenPeriod() {
    }

    @Test
    void shouldReturnFalseWhenRoomIsUnavailableInGivenPeriod() {
        // given
        BookingRequest request = new BookingRequest(NOT_EXISTING_ROOM_ID, new DateCriteria(CHECK_IN, CHECK_OUT), EMAIL);

        // when, then
        assertThrows(RoomNotFoundException.class, () -> sut.createBooking(request));
    }

//todo add tests for delete

    void initializeRooms() {
        given(roomRepository.findById(eq(ROOM_ONE.getId()))).willReturn(Optional.of(ROOM_ONE));
        given(roomRepository.findById(eq(NOT_EXISTING_ROOM_ID))).willReturn(Optional.empty());
    }

    void expectSavedBooking(Booking expectedBooking) {
        given(bookingRepository.save(any())).willReturn(expectedBooking);
    }
}