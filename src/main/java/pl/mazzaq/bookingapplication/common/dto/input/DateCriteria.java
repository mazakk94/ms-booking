package pl.mazzaq.bookingapplication.common.dto.input;

import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;

@Value
public class DateCriteria implements Comparable<DateCriteria>{

    @DateTimeFormat(iso = DATE)
    Date checkIn;

    @DateTimeFormat(iso = DATE)
    Date checkOut;


    /**
     * there is a collision between DateCriteria when result is 0.
     */
    @Override
    public int compareTo(DateCriteria dateCriteria) {
        DateCriteria earlier;
        DateCriteria later;
        int compare = this.checkIn.compareTo(dateCriteria.getCheckIn());
        if (compare == 0) {
            return 0;
        } else if (compare < 0) {
            earlier = this;
            later = dateCriteria;
        } else {
            earlier = dateCriteria;
            later = this;
        }
        boolean collision = later.getCheckIn().compareTo(earlier.getCheckOut()) < 0;
        if (collision) {
            return 0;
        } else {
            return compare;
        }
    }
}
