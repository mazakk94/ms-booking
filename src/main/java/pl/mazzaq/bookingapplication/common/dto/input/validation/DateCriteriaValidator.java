package pl.mazzaq.bookingapplication.common.dto.input.validation;

import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateCriteriaValidator implements ConstraintValidator<DateCriteriaConstraint, DateCriteria> {

    @Override
    public boolean isValid(DateCriteria dateCriteria, ConstraintValidatorContext context) {
        if (dateCriteria.getCheckIn() == null || dateCriteria.getCheckOut() == null) {
            return false;
        }
        return dateCriteria.getCheckOut().after(dateCriteria.getCheckIn());
    }
}
