package pl.mazzaq.bookingapplication.common.dto.input.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateCriteriaValidator.class)
@Target({ElementType.PARAMETER, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DateCriteriaConstraint {
    String message() default "Invalid date provided";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}