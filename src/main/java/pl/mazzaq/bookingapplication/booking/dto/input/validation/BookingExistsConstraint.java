package pl.mazzaq.bookingapplication.booking.dto.input.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = BookingExistsValidator.class)
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface BookingExistsConstraint {
    String message() default "Invalid booking ID provided";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}