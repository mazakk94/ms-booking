package pl.mazzaq.bookingapplication.booking.dto.input.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mazzaq.bookingapplication.booking.repository.BookingRepository;
import pl.mazzaq.bookingapplication.rooms.repository.RoomRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class BookingExistsValidator implements ConstraintValidator<BookingExistsConstraint, Long> {

    private final BookingRepository bookingRepository;

    @Autowired
    public BookingExistsValidator(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @Override
    public boolean isValid(Long roomId, ConstraintValidatorContext context) {
        if (roomId == null) {
            return false;
        }
        return bookingRepository.existsById(roomId);
    }
}
