package pl.mazzaq.bookingapplication.booking.dto.input;

import lombok.Value;
import pl.mazzaq.bookingapplication.rooms.dto.validation.RoomAvailableConstraint;
import pl.mazzaq.bookingapplication.rooms.dto.validation.RoomExistsConstraint;
import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;
import pl.mazzaq.bookingapplication.common.dto.input.validation.DateCriteriaConstraint;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Value
@RoomAvailableConstraint
public class BookingRequest {

    @NotNull
    @RoomExistsConstraint
    Long roomId;

    @DateCriteriaConstraint
    DateCriteria dateCriteria;

    @NotNull
    @Email
    String email;
}