package pl.mazzaq.bookingapplication.booking.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import pl.mazzaq.bookingapplication.booking.dto.input.BookingRequest;
import pl.mazzaq.bookingapplication.booking.dto.input.validation.BookingExistsConstraint;
import pl.mazzaq.bookingapplication.booking.repository.BookingRepository;
import pl.mazzaq.bookingapplication.booking.repository.model.Booking;
import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;
import pl.mazzaq.bookingapplication.common.rest.exception.RoomNotFoundException;
import pl.mazzaq.bookingapplication.rooms.repository.RoomRepository;
import pl.mazzaq.bookingapplication.rooms.repository.model.Room;

import javax.validation.Valid;

@Validated
@Service
public class BookingService {

    private final BookingRepository bookingRepository;
    private final RoomRepository roomRepository;

    @Autowired
    public BookingService(BookingRepository bookingRepository, RoomRepository roomRepository) {
        this.bookingRepository = bookingRepository;
        this.roomRepository = roomRepository;
    }

    @Transactional
    @SneakyThrows(RoomNotFoundException.class)
    public Booking createBooking(@Valid BookingRequest bookingRequest) {
        DateCriteria dateCriteria = bookingRequest.getDateCriteria();
        Room room = roomRepository.findById(bookingRequest.getRoomId()).orElseThrow(RoomNotFoundException::new);

        Booking booking = Booking.builder()
                .checkIn(dateCriteria.getCheckIn())
                .checkOut(dateCriteria.getCheckOut())
                .email(bookingRequest.getEmail())
                .room(room)
                .build();

        return bookingRepository.save(booking);
    }

    public void deleteBooking(@BookingExistsConstraint Long id) {
        bookingRepository.deleteById(id);
    }
}