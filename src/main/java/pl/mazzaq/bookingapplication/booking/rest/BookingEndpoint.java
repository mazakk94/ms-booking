package pl.mazzaq.bookingapplication.booking.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pl.mazzaq.bookingapplication.booking.dto.input.BookingRequest;
import pl.mazzaq.bookingapplication.booking.service.BookingService;

import javax.validation.constraints.NotNull;

@Validated
@RestController
@RequestMapping("/api/booking")
public class BookingEndpoint {

    private final BookingService bookingService;

    @Autowired
    public BookingEndpoint(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping
    public ResponseEntity<Long> createBooking(@RequestBody BookingRequest bookingRequest) {
        Long bookingId = bookingService.createBooking(bookingRequest).getId();
        return new ResponseEntity<>(bookingId, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBooking(@PathVariable @NotNull Long id) {
        bookingService.deleteBooking(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

