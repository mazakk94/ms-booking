package pl.mazzaq.bookingapplication.booking.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mazzaq.bookingapplication.booking.repository.model.Booking;

import java.util.List;

@Repository
public interface BookingRepository extends CrudRepository<Booking, Long> {
    List<Booking> findByRoomId(long roomId);
}
