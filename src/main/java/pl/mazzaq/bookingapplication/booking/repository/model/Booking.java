package pl.mazzaq.bookingapplication.booking.repository.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;
import pl.mazzaq.bookingapplication.rooms.repository.model.Room;

import javax.persistence.*;
import java.util.Date;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String email;
    Date checkIn;
    Date checkOut;

    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false)
    Room room;

    public DateCriteria getDateCriteria() {
        return new DateCriteria(checkIn, checkOut);
    }
}
