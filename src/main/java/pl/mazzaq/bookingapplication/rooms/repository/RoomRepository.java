package pl.mazzaq.bookingapplication.rooms.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mazzaq.bookingapplication.rooms.repository.model.Room;

@Repository
public interface RoomRepository extends CrudRepository<Room, Long> {
}
