package pl.mazzaq.bookingapplication.rooms.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;
import pl.mazzaq.bookingapplication.common.dto.input.validation.DateCriteriaConstraint;
import pl.mazzaq.bookingapplication.rooms.dto.output.RoomsResponse;
import pl.mazzaq.bookingapplication.rooms.repository.model.Room;
import pl.mazzaq.bookingapplication.rooms.service.RoomsService;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/api/rooms")
public class RoomsEndpoint {

    private final RoomsService roomsService;

    @Autowired
    public RoomsEndpoint(RoomsService roomsService) {
        this.roomsService = roomsService;
    }

    @GetMapping
    public RoomsResponse getAvailableRooms(@DateCriteriaConstraint @Valid DateCriteria dateCriteria) {
        List<Room> availableRooms = roomsService.getAvailableRooms(dateCriteria);
        return new RoomsResponse(availableRooms, dateCriteria.getCheckIn(), dateCriteria.getCheckOut());
    }
}

