package pl.mazzaq.bookingapplication.rooms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mazzaq.bookingapplication.rooms.dto.validation.RoomAvailableValidator;
import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;
import pl.mazzaq.bookingapplication.rooms.repository.RoomRepository;
import pl.mazzaq.bookingapplication.rooms.repository.model.Room;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

@Service
public class RoomsService {

    private final RoomRepository roomRepository;
    private final RoomAvailableValidator roomAvailableValidator;

    @Autowired
    public RoomsService(RoomRepository roomRepository, RoomAvailableValidator roomAvailableValidator) {
        this.roomRepository = roomRepository;
        this.roomAvailableValidator = roomAvailableValidator;
    }

    public List<Room> getAvailableRooms(@Valid DateCriteria dateCriteria) {
        return newArrayList(roomRepository.findAll())
                .stream()
                .filter(room -> roomAvailableValidator.isRoomAvailable(room.getId(), dateCriteria))
                .collect(Collectors.toList());
    }
}