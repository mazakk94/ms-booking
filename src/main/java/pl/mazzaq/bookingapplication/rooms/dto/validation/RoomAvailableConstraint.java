package pl.mazzaq.bookingapplication.rooms.dto.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = RoomAvailableValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RoomAvailableConstraint {
    String message() default "Invalid room ID provided";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
