package pl.mazzaq.bookingapplication.rooms.dto.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mazzaq.bookingapplication.rooms.repository.RoomRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class RoomExistsValidator implements ConstraintValidator<RoomExistsConstraint, Long> {

    private final RoomRepository roomRepository;

    @Autowired
    public RoomExistsValidator(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public boolean isValid(Long roomId, ConstraintValidatorContext context) {
        if (roomId == null) {
            return false;
        }
        return roomRepository.existsById(roomId);
    }
}
