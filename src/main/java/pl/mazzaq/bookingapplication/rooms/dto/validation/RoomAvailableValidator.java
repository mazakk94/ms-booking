package pl.mazzaq.bookingapplication.rooms.dto.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.mazzaq.bookingapplication.booking.dto.input.BookingRequest;
import pl.mazzaq.bookingapplication.booking.repository.BookingRepository;
import pl.mazzaq.bookingapplication.booking.repository.model.Booking;
import pl.mazzaq.bookingapplication.common.dto.input.DateCriteria;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@Component
public class RoomAvailableValidator implements ConstraintValidator<RoomAvailableConstraint, BookingRequest> {

    private final BookingRepository bookingRepository;

    @Autowired
    public RoomAvailableValidator(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @Override
    public boolean isValid(BookingRequest request, ConstraintValidatorContext context) {
        return isRoomAvailable(request.getRoomId(), request.getDateCriteria());
    }

    public boolean isRoomAvailable(long roomId, DateCriteria dateCriteria) {
        List<Booking> bookingList = bookingRepository.findByRoomId(roomId);

        Predicate<Booking> bookingWithDateInDateCriteria = booking -> booking.getDateCriteria().compareTo(dateCriteria) == 0;
        Optional<Booking> existingBooking = bookingList.stream()
                .filter(bookingWithDateInDateCriteria)
                .findAny();
        return existingBooking.isEmpty();
    }
}
