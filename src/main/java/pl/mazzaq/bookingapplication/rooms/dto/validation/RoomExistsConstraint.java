package pl.mazzaq.bookingapplication.rooms.dto.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = RoomExistsValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RoomExistsConstraint {
    String message() default "Invalid room ID provided";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}