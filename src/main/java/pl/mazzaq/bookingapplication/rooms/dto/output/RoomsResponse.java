package pl.mazzaq.bookingapplication.rooms.dto.output;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Value;
import pl.mazzaq.bookingapplication.rooms.repository.model.Room;

import java.util.Date;
import java.util.List;

@Value
public class RoomsResponse {

    List<Room> availableRooms;

    @JsonFormat(pattern = "yyyy-MM-dd")
    Date checkIn;

    @JsonFormat(pattern = "yyyy-MM-dd")
    Date checkOut;
}
